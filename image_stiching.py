import cv2
import numpy as np
import scipy
from matplotlib import pyplot as plt


def getPoints_SIFT(im1, im2, ratio=0.3):
    """
    given im1,im2 and ratio, return p1,p2 intrest points using SIFT desc
    """
    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()
    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(im1, None)
    kp2, des2 = sift.detectAndCompute(im2, None)

    # FLANN parameters (fast knn)
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)  # or pass empty dictionary
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1, des2, k=2)

    # Need to draw only good matches, so create a mask
    matchesMask = [[0, 0] for i in range(len(matches))]

    # ratio test as per Lowe's paper
    p1 = []
    p2 = []
    for i, (m, n) in enumerate(matches):
        if m.distance < ratio * n.distance:
            matchesMask[i] = [1, 0]
            p1.append(kp1[m.queryIdx].pt)
            p2.append(kp2[m.trainIdx].pt)

    p1 = np.array(p1)
    p2 = np.array(p2)

    """
    draw_params = dict(matchColor=(0, 255, 0),singlePointColor=(255, 0, 0),matchesMask=matchesMask,flags=0)
    img3 = cv2.drawMatchesKnn(im1, kp1, im2, kp2, matches, None, **draw_params)
    plt.imshow(img3, ), plt.show()
    """
    return p1, p2


def tight_bound(im):
  """
  get rid of black wrap of a given image and return it with the smallest triangel
      """
  g = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
  x_min = im.shape[1]
  x_max = 0
  y_min = im.shape[0]
  y_max = 0
  for i in range(im.shape[1]):
    for j in range(im.shape[0]):
        if (g[j, i] > 0):
            y_min = j if (j < y_min) else y_min
            x_min = i if (i < x_min) else x_min
            x_max = i if (i > x_max) else x_max
            y_max = j if (j > y_max) else y_max
  out = im[y_min:y_max,x_min:x_max:]
  return out

def main():
    im1 = cv2.imread('data/right.jpeg')
    im2 = cv2.imread('data/left.jpeg')

    im1_rgb = cv2.cvtColor(im1, cv2.COLOR_BGR2RGB)
    im2_rgb = cv2.cvtColor(im2, cv2.COLOR_BGR2RGB)

    p1_sift, p2_sift = getPoints_SIFT(im1, im2, ratio=0.19)
    h1, w1, _ = im1.shape
    h2, w2, _ = im2.shape
    H, mask = cv2.findHomography(p1_sift, p2_sift, cv2.RANSAC, 5.0)
    out_img = cv2.warpPerspective(im1, H, (w1+w2, h1))


    out_img[: im2.shape[0] ,: im2.shape[1] ,:  ] = im2
    #image stitching

    out_img = tight_bound(out_img)
    out_img_rgb = cv2.cvtColor(out_img, cv2.COLOR_BGR2RGB)

    plt.subplot(1, 1, 1)
    plt.axis('off')
    plt.imshow(out_img_rgb)

    plt.show()
if __name__ == '__main__':
    main()